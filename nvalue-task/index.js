const cheerio = require('cheerio');
const getUrlData = require('./getURLData');
const xl = require('excel4node');

const process = async () => {
    let data = await getUrlData('https://www.ok-power.de/fuer-strom-kunden/anbieter-uebersicht.html');
    let company = [];

    const $ = await cheerio.load(data);

    sheetLength = await $('#anbieterliste div table');

    let arr = [];
    await sheetLength.each(function (i, elem) {
        arr[i] =
            {
                company: $(('.row_0 .col_0'), $(this)).text(),
                Address: $(('.row_2 .col_0'), $(this)).text() + ' ' + $(('.row_3 .col_0'), $(this)).text(),
                phone: $(('.row_2 .col_1'), $(this)).text(),
                fax: $(('.row_3 .col_1'), $(this)).text(),
                email: $(('.row_4 .col_1'), $(this)).text(),
                'contact person': $(('.row_5 .col_1'), $(this)).text(),
                website: $(('.row_6 .col_1'), $(this)).text(),
            }
    });
    await generateXLSX(arr);
};

const generateXLSX = (arrOfObjects) => {
    let wb = new xl.Workbook();
    let ws = wb.addWorksheet('Contacts');

    let style = wb.createStyle({
        font: {
            bold: true,
            size: 18,
        }

    });

    for (let i = 1; i < arrOfObjects.length + 1; i++) {
        const columns = arrOfObjects[i - 1];

        j = 1
        Object.keys(columns).forEach(function (key) {
            if (i === 1) {
                ws.cell(i, j).string(key).style(style);
                ws.cell(i + 1, j).string(columns[key]);
                j++;
            } else {
                ws.cell(i + 1, j).string(columns[key]);
                j++;
            }
        })
    }
    wb.write('Excel.xlsx');
}

process();