const fetch = require('node-fetch');

const getUrlData = async (utl) => {
    let resp = await fetch(utl);
    return data = await resp.text();
    //console.log(JSON.stringify(data, null, "\t"))
};

module.exports = getUrlData;

