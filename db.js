if (!global.__base) {
    /* FOR TESTING PURPOSE ONLY */
    global.__base = require('path').join(__dirname, '../');
}

const mysql = require('mysql');
const utils = require(`${__base}/utils`);
const C = require(`${__base}/constants`);
const config = {
    databases: {
        default: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'mydatabase1',
            timezone: 'utc'
        },
        users: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'users_database',
            timezone: 'utc'
        }
    },
    settings: {
        timeout: 30000
    }
};
/**
     * DB IIFE 
     * @return {Object} with status(state) of every function.
     */
const Db = (function () {

    let errors = [];

    /**
     * Create mysql conneciton
     * @param  {String} name Name of DB from config object
     * @return {promise} with connection object or err object
     */
    function _getConnection(name) {


        let defer = utils.async.defer();

        let configuration = typeof name === 'string' && config.databases[name] ? config.databases[name] : name;

        if (!configuration) {
            defer.reject({
                type: C.errors.CONFIG_MISSING,
                error: {
                    message: `Configuration with name "${name}" not found!`
                }
            });
            return defer;
        }

        let connection = mysql.createConnection(configuration);
        connection.connect(err => {

            if (err) {
                return defer.reject({
                    type: C.errors.CONNECTION,
                    error: err
                });
            }

            defer.resolve(connection);
        });

        return defer;
    }

    /**
     * get database credentials from config object
     * @param  {String} connectionName Name of DB from config object
     * @return {Object} with database credentials
     */
    function _getCredentials(connectionName) {
        return config.databases[connectionName] ? config.databases[connectionName] : null;
    }


    /**
     * Start transaction function in mySql
     * @param  {Object} connection mysql connection object
     * @return {Promise} bool or err object
     */

    function _startT(connection) {

        let defer = utils.async.defer();

        connection.beginTransaction(err => {
            if (err) {
                return defer.reject({
                    type: C.errors.BEGIN_TRANSACTION,
                    error: err
                });
            }
            return defer.resolve(true);
        });

        return defer;
    }


    /**
     * rollback or commit transacrion
     * @param  {Object} connection mysql connection object
     * @param  {String} flag type of transaction
     * @return {Promise} if promise is fulflled return true or err object
     */
    function _endT(connection, flag) {

        let defer = utils.async.defer();
        let transactionMethod = flag ? 'commit' : 'rollback';

        if (!connection) {
            defer.resolve(true);
        } else {
            connection[transactionMethod](err => {
                if (err) {
                    return defer.reject({
                        type: C.errors.END_TRANSACTION,
                        error: err
                    });
                }
                return defer.resolve(true);
            });
        }

        return defer;
    }

    /**
     * rollback or commit transacrion
     * @param  {Object} conn mysql connection object
     * @param  {String} sql mysql query string
     * @param  {String} params query values arguments
     * @param  {Number} timeout query timeout
     * @return {Promise} with objects of results and fields or err object
     */
    function _query(conn, sql, params, timeout) {

        let defer = utils.async.defer();

        conn.query({
            sql: sql,
            values: params,
            timeout: timeout || config.settings.timeout
        }, function (err2, results, fields) {

            if (err2) {
                return defer.reject({
                    type: C.errors.QUERY,
                    error: err2
                });
            }

            defer.resolve({ data: results, fields: fields });
        });

        return defer;
    }

    /**
     * return erros
     * @return {Array} Array of errors
     */
    function _getErrors() {
        return errors;
    }

    /**
     * return last error
     * @return {String} last error
     */
    function _getLastError() {
        return errors[errors.length] || null;
    }

    /**
     * Clear errors array
     * @return {Array} empty errors array
     */
    function _clearErros() {
        errors = [];
        return this;
    }

    /**
     * Closing mysql connection
     * @param  {Object} conn mysql connection object
     * @return {Promise} with bool or error msg
     */
    function _disconnect(conn) {

        let defer = utils.async.defer();

        if (!conn) {
            defer.resolve(true);
        } else {
            conn.end(err => err ? defer.reject(err) : defer.resolve(true));
        }

        return defer;
    }

    /**
     * checking mysql connection status
     * @param  {Object} conn mysql connection object
     * @return {Bool}
     */
    function _isConnected(connection) {
        return connection && connection.state && ['connected', 'authenticated'].indexOf(connection.state) !== -1;
    }

    return {
        isConnected: _isConnected,
        getConnection: _getConnection,
        getCredentials: _getCredentials,
        startT: _startT,
        endT: _endT,
        getErrors: _getErrors,
        getLastError: _getLastError,
        clearErros: _clearErros,
        query: _query,
        disconnect: _disconnect
    }

})();


exports = module.exports = Db;





